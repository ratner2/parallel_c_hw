/*
 * homework_main.c
 *
 *  Created on: Jul 22, 2015
 *      Author: bratner
 */
#include "questions.h"

/*
 *
 *  By Boris Ratner, 303470256
 *
 *  -- Uncomment the right main function for the paragraph you would like to
 *  implement.
 *  -- Compile and run for parallel fun.
 *
 */

int main(int argc, char * argv[]) {
	//return hw1_3a(argc, argv);
	return hw1_3b(argc, argv);
}

