/*
 ============================================================================
 Name        : homework1.c
 Author      : Boris Ratner
 Version     :
 Copyright   : All rights reserved 2015
 Description : Calculate Pi in MPI
 ============================================================================
 */
#include <mpi.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "questions.h"
/*
 * b) Start two processes.
 * The first process sends some number to the second, which multiplies it by some random number and sends
 * the result to the first process.
 * Each process displays its rank and all relevant information – the data send and received.
 */

int
hw1_3b(int argc, char *argv[])
{
	int			my_rank;		/* rank of process */
	int			num_procs;		/* number of processes */
	int			source;			/* rank of sender */
	int			dest = 0;		/* rank of receiver */
	int			tag = 0;		/* tag for messages */
	char		message[100];	/* storage for message */
	MPI_Status	status ;		/* return status for receive */

	/* start up MPI */

	MPI_Init(&argc, &argv);

	/* find out process rank */
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

	/* find out number of processes */
	MPI_Comm_size(MPI_COMM_WORLD, &num_procs);

	srandom(abs(MPI_Wtime()*1000));

	if(num_procs < 2) {
		fprintf(stderr, "ERROR: At least 2 processes are required to run this solution!\n");
	}


	if (my_rank == 0) {
		MPI_Status status;
		int my_out = random()%100;
		MPI_Send(&my_out, 1, MPI_INT, 1, 0, MPI_COMM_WORLD);
		printf("Sent out %d to P_1\n", my_out);
		MPI_Recv(&my_out, 1, MPI_INT, 1, 0, MPI_COMM_WORLD, &status);
		printf("Received %d back from P_1\n", my_out);
	} else if(my_rank == 1) {
		int my_input;
		MPI_Status status;
		MPI_Recv(&my_input, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
		my_input *= random() % 100;
		MPI_Send(&my_input, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
	}

	/* shut down MPI */
	MPI_Finalize();

	return 0;
}
