/*
 * questions.h
 *
 *  Created on: Jul 22, 2015
 *      Author: bratner
 */

#ifndef QUESTIONS_H_
#define QUESTIONS_H_

int hw1_3a(int argc, char * argv[]);
int hw1_3b(int argc, char * argv[]);
int hw1_3c(int argc, char * argv[]);
int hw1_3d(int argc, char * argv[]);
int hw1_3e(int argc, char * argv[]);

#endif /* QUESTIONS_H_ */
