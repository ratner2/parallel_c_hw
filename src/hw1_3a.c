/*
 ============================================================================
 Name        : homework1.c
 Author      : Boris Ratner
 Version     :
 Copyright   : All rights reserved 2015
 Description : Calculate Pi in MPI
 ============================================================================
 */
#include <mpi.h>
#include <stdio.h>
#include <string.h>
#include "questions.h"
/*
 * a) Start a number of processes. The only job of each process is to display its rank.
 */

int 
hw1_3a(int argc, char *argv[])
{
	int			my_rank;		/* rank of process */
	int			num_procs;		/* number of processes */

	/* start up MPI */
	MPI_Init(&argc, &argv);

	/* find out process rank */
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank); 

	/* find out number of processes */
	MPI_Comm_size(MPI_COMM_WORLD, &num_procs); 

	printf("Running on rank: %d out of %d nodes\n",my_rank, num_procs);
	/* shut down MPI */
	MPI_Finalize(); 

	return 0;
}
